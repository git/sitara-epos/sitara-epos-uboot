/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <stdarg.h>
#include <config.h>
#include <image.h>

#include <asm/arch/hardware.h>
#include <asm/arch/sys_proto.h>


#define SEC_SVC_MAX_PARAMS	4

/**
 * secure_service() - Request a secure service.
 * @req: Identifies the secure service being requested
 * @status: Status returned by the secure service
 * @nargs: Number of parameters to be passed to the secure service.
 * @arg1: First parameter
 * @arg2: Second parameter
 * @arg3: Third parameter
 * @arg4: Forth parameter
 *
 * This function causes a context switch to secure world so that requested
 * secure service can be provided.
 *
 * Returns: Always 0. Error checks and codes will be added later.
 */
int secure_service(unsigned int req, unsigned int *status,
		u32 nargs, u32 arg1, u32 arg2, u32 arg3, u32 arg4)
{
	u32 params[SEC_SVC_MAX_PARAMS + 1];

	params[0] = nargs;
	params[1] = arg1;
	params[2] = arg2;
	params[3] = arg3;
	params[4] = arg4;

#ifndef CONFIG_SYS_DCACHE_OFF
	flush_dcache_range((unsigned long)params, (unsigned long)&params[4]);
#endif

	*status = do_omap_emu_romcode_call(req, (u32)(params));

	return 0;
}


extern unsigned int rom_hal_call(unsigned int req, unsigned int pid,
                unsigned int flags, va_list list);

/**
 * secure_service2() - Request a secure service.
 * @req: Identifies the secure service being requested
 * @pid: Process ID
 * @status: Status returned by the secure service
 * @flags: FLags associated with the secure service
 * @list: Variable list of arguments passed to the requested service. First
 * entry in the list indicates number of arguments in the list.
 *
 * This function causes a context switch to secure world so that requested
 * secure service can be provided.
 *
 * The argument "pid" is currently ignored. It is kept for future use.
 *
 * Returns: Always 0. Error checks and codes will be added later.
 */
int secure_service2(unsigned int req, unsigned int pid, unsigned int *status,
                unsigned int flags, ...)
{
        va_list list;

        va_start(list, flags);
        *status = rom_hal_call(req, pid, flags, list);
        va_end(list);

        return 0;
}

#ifdef CONFIG_HS_AUTH_BOOT
#ifndef CONFIG_AUTH_UCS
/**
 * verify_image - verify image with authentication certificate
 * attached with it
 *
 * @img_addr_p: image start address as input parameter
 *
 * This function invokes the secure monitor API which will authenticate the
 * image located in RAM.
 *
 * Assumes that image is already present in RAM.
 *
 * Note: Since the signature is at the end of the image, the image start address
 * remains same after authentication as passed by the caller and thus, value
 * referenced by img_addr_p is not changed by this function. This parameter is
 * kept as in/out to maintain caller agnostic to the verification method and
 * image format and expect the image pointer to point to final verified image
 * ready to be executed. Same reason applies for keeping "decrypt" in the name.
 *
 * To summarize, the @img_addr_p returned will be same as input by the caller
 *
 * returns:
 *     0 on authentication success, non-zero value on failure
 *
 *     Error codes:
 *	1	=> authentication failure
 *	-1	=> error parsing image header
 *	-2	=> non 32-byte aligned image address
 */
int verify_image(u32 *img_addr_p)
{
	u32	img_addr = *img_addr_p;
	u32	h_size, d_size, sig_addr;
	int	image_format;
	u32	ret;

	/* get header size */
	h_size = image_get_header_size();

#if defined(CONFIG_FIT)
	if (sizeof(struct fdt_header) > h_size)
		h_size = sizeof(struct fdt_header);
#endif

#ifdef CONFIG_SPL_BUILD
	/*
	 * SPL supports U-Boot legacy image or image w/o header - this means we
	 * may get to verify an image with no header at all. In such case,
	 * the authentication cannot be done as we don't have the image size
	 * information.
	 *
	 * Hence, exit if image doesn't have valid header (magic).
	 */
	if (!image_check_magic((const image_header_t *)img_addr)) {
		printf ("   Bad image header\n");
		return -1;
	}

	image_format = IMAGE_FORMAT_LEGACY;
#else
	image_format = genimg_get_format((void *)img_addr);
#endif

	/* get data size */
	switch (image_format) {
		case IMAGE_FORMAT_LEGACY:
			d_size = image_get_data_size((const image_header_t *)img_addr);
			debug("\n   U-Boot legacy format image found at 0x%08x,"
					" size 0x%08x\n", img_addr, d_size);
			break;
#if defined(CONFIG_FIT)
		case IMAGE_FORMAT_FIT:
			d_size = fit_get_size((const void *)img_addr) - h_size;
			debug("\n   FIT/FDT format image found at 0x%08x, size 0x%08lx\n",
					img_addr, d_size);
			break;
#endif
		default:
			printf("\n   No valid image found at 0x%08x\n", img_addr);
			return -1;
	}

	/*
	 * Prepare secure API parameters
	 *	- U-Boot mkimage generated header is included for authentication
	 *	- Certificate lies at the end of the image
	 */
	d_size += h_size;
	sig_addr = img_addr + d_size;

	/* Check if image load address is 32-byte aligned */
	if (0 != (img_addr % 0x20)) {
		printf("\n   ERROR: Image load address is not 32-byte aligned"
				" (0x%08x)\n", img_addr);
		return -2;
	}

	debug("\nInvoking API for verifying image @%#x with size %#x\n",
			img_addr, d_size);

#ifndef CONFIG_SYS_DCACHE_OFF
	flush_dcache_range(img_addr, sig_addr + AM43XX_AUTH_SIG_SIZE);
#endif

	secure_service(SECURE_SRV_VERIFYIMAGE, &ret, 4, img_addr, d_size,
			sig_addr, KEY_RIGHTS_VERIFY_ISW);

	return ret;
}
#else
/**
 * verify_image - verify image with authentication certificate
 * attached with it
 *
 * @img_addr_p: image start address as input parameter. Updated ion
 * authentication success with actual image address after skipping header.
 *
 * This function invokes the secure monitor API which will authenticate the
 * image located in RAM.
 *
 * Assumes that image is already present in RAM.
 *
 * Since the signed image will have the authentication signature at the
 * begining we are having a different function to handle this, which
 * passes the correct paramters to the secure API
 *
 * returns:
 *     0 on authentication success, non-zero value on failure
 *
 *     Error codes:
 *	1	=> authentication failure
 *	-1	=> error parsing image header
 *	-2	=> non 32-byte aligned image address
 */
int verify_image(ulong *img_addr_p)
{
       ulong   img_addr;
       ulong   h_size, d_size, sig_addr;
       int     image_format;
       u32     ret;

       sig_addr = *img_addr_p;

       img_addr = sig_addr + AM43XX_UCS_HDR_SIZE;
       /* get header size */
        h_size = image_get_header_size();

#ifdef CONFIG_SPL_BUILD

      if (!image_check_magic((const image_header_t *)img_addr)) {
               printf ("   Bad image header\n");
               return -1;
       }
#endif

      /* Check if image load address is 32-byte aligned */
      if (0 != (img_addr % 0x20)) {
	      printf("\n   ERROR: Image load address is not 32-byte aligned"
			      " (0x%08x)\n", img_addr);
	      return -2;
      }

       d_size = image_get_data_size((const image_header_t *)img_addr) + h_size;

#ifndef CONFIG_SYS_DCACHE_OFF
       flush_dcache_range(sig_addr, img_addr + d_size);
#endif

       secure_service(SECURE_SRV_VERIFYIMAGE_UCS,
                        &ret, 4, img_addr, d_size,
                        KEY_RIGHTS_VERIFY_UCS,sig_addr);

       if (!ret)
               *img_addr_p = sig_addr + AM43XX_UCS_HDR_SIZE;

       return ret;
}
#endif
#endif
